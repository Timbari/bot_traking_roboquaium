import cv2
import numpy as np
import matplotlib.pyplot as plt

def segment_image(img):
  img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
  lower_orange = np.array([0,130,50])
  upper_orange = np.array([30,255,255])
  mask = cv2.inRange(img_hsv, lower_orange, upper_orange)
  kernel = np.ones((21,21),np.uint8)
  open = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
  closing = cv2.morphologyEx(open, cv2.MORPH_CLOSE, kernel)
  return closing

def duplicate(frame, T):
  closing = segment_image(frame)
  closing  = cv2.warpAffine(closing, T, frame_size)
  frame_affine = cv2.warpAffine(frame, T, frame_size)
  mask_inv = cv2.bitwise_not(closing)
  img1_bg = cv2.bitwise_and(frame, frame,mask = mask_inv)
  img = cv2.bitwise_and(frame_affine, frame_affine, mask=closing)
  test = cv2.add(img1_bg, img)
  closing = segment_image(test)
  return test, closing
cap = cv2.VideoCapture("")



frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
frame_size = (980,900)
fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
segment = cv2.VideoWriter('segment.mp4', cv2.VideoWriter_fourcc(*'mp4v'), 20, frame_size)

track = cv2.VideoWriter('track.mp4', fourcc, 30, frame_size)
prev_angle = 90
cx_prev = 0
cy_prev = 0
dx_prev = 0
dy_prev = 0
toggle = False
count = 0
curr_max = -1000
buffer = []
frame_buffer = []
buffer_size = 130
if (cap.isOpened()== False): 
  print("Error opening video stream or file")

while(cap.isOpened()):

  ret, frame = cap.read()
  count = count + 2
  if ret == True and count%2 == 0:
    
    frame  = frame[650:1550, 100:1080]
    # frame_buffer.append(frame.copy())
    closing = segment_image(frame)
    result = cv2.bitwise_and(frame, frame, mask=closing)
    # buffer.append(closing.copy())
    # if len(buffer) == buffer_size:
    #   mask = buffer.pop(0)
    #   old_frame = frame_buffer.pop(0)
    #   mask_inv = cv2.bitwise_not(mask)
    #   img1_bg = cv2.bitwise_and(frame, frame,mask = mask_inv)
    #   img1_bg = cv2.rotate(img1_bg,  cv2.ROTATE_180)
    #   img = cv2.bitwise_and(old_frame, old_frame, mask=mask)
    #   img = cv2.rotate(img,  cv2.ROTATE_180)
    #   frame = cv2.add(img1_bg, img)
    #   closing = segment_image(frame)
    T = np.float32([[1, 0, 150], [0, 1, 150]])
    frame, closing = duplicate(frame, T)

    # closing  = cv2.warpAffine(closing, T, frame_size)
    # frame_affine = cv2.warpAffine(frame, T, frame_size)
    # mask_inv = cv2.bitwise_not(closing)
    # img1_bg = cv2.bitwise_and(frame, frame,mask = mask_inv)
    # img = cv2.bitwise_and(frame_affine, frame_affine, mask=closing)
    # frame = cv2.add(img1_bg, img)
    # closing = segment_image(frame)
    # result = cv2.bitwise_and(frame, frame, mask=closing)
    output = frame.copy()
    contours,_= cv2.findContours(closing,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    # cv2.drawContours(output, contours, -1, (0, 255, 0),  2, cv2.LINE_AA)
    max_contour = contours[0]
    
    for contour in contours:
      # print(cv2.contourArea(contour))
      if cv2.contourArea(contour)>10000 and cv2.contourArea(contour) < 30000:
        max_contour = contour
      elif cv2.contourArea(contour)>10000:
        M=cv2.moments(contour)
        cx=int(M['m10']//M['m00'])
        cy=int(M['m01']//M['m00'])
        test = frame.copy()
        # cv2.circle(test, (cx,cy), 40, (0,0,0), -1)
        [vx,vy,x,y] = cv2.fitLine(contour, cv2.DIST_L2,0,0.01,0.01)
        temp = vx
        vX = -vy
        vY = temp
        cX = x + vX * length
        cY = y + vY * length
        dX = x - vX * length
        dY = y - vY * length
        cv2.line(test, (int(cX),int(cY)), (int(dX),int(dY)),(0,0,0), 50)
        closing = segment_image(test)
        contours_test,_= cv2.findContours(closing,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE) 
        cv2.drawContours(output, contours_test, -1, (255, 0, 0),  2, cv2.LINE_AA)
        for test_contour in contours_test:
          M=cv2.moments(test_contour)
          cx=int(M['m10']//M['m00'])
          cy=int(M['m01']//M['m00'])
          cv2.circle(output, (cx,cy),20,(255,0,0),-1)
        cv2.imshow("testr", test)
    contour = max_contour
    
    # approx=cv2.approxPolyDP(contour, 0.01*cv2.arcLength(contour,True),True)

    # rect = cv2.minAreaRect(approx)
    # box = cv2.boxPoints(rect) # cv2.boxPoints(rect) for OpenCV 3.x
    # box = np.int0(box)
    # cv2.drawContours(frame,[box],0,(0,0,255),2)
    # x,y,w,h=cv2.boundingRect(approx)
    # cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),4)
    # rows,cols = frame.shape[:2]
    [vx,vy,x,y] = cv2.fitLine(contour, cv2.DIST_L2,0,0.01,0.01)
    length =40
    mag = np.sqrt(vx*vx + vy*vy)
    vX = vx / mag
    vY = vy / mag
    temp = vX
    vX = -vY
    vY = temp
    if np.sign(vX) == 1 and np.sign(vY) == 1:
      cv2.putText(output, "Q1", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
    elif np.sign(vX) == -1 and np.sign(vY) == 1:
      cv2.putText(output, "Q2", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
    elif np.sign(vX) == -1 and np.sign(vY) == -1:
      cv2.putText(output, "Q3", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
    else:
      cv2.putText(output, "Q4", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

    cX = x + vX * length
    cY = y + vY * length
    dX = x - vX * length
    dY = y - vY * length

    if toggle:
      eX = dX - cX
      eY = dY - cY
      curr_angle  = np.arctan2(eY, eX)
    else:
      eX = cX - dX
      eY = cY - dY
      curr_angle  = np.arctan2(eY, eX)
    # if np.sqrt((vX*length-cx_prev)**2 + (vY*length-cy_prev)**2) < 7 or (cx_prev == 0 and cy_prev ==0):
    #   cv2.arrowedLine(output, (int(cX),int(cY)), (int(dX),int(dY)),(0,255,0), 10 , tipLength=.4)
    #   cx_prev = vX*length
    #   cy_prev =vY * length
    # else:
    #   cv2.arrowedLine(output, (int(dX),int(dY)),(int(cX),int(cY)),(0,255,0), 10 , tipLength=.4)
    if np.abs(curr_angle - prev_angle) > curr_max and np.abs(curr_angle - prev_angle) < 1.57:
      # print(np.abs(curr_angle - prev_angle))
      curr_max = np.abs(curr_angle - prev_angle)
    if (prev_angle == 90  or np.abs(curr_angle - prev_angle) <1.54):
      pass      
    else:
      toggle = ~toggle
      if toggle:
        eX = dX - cX
        eY = dY - cY
        curr_angle  = np.arctan2(eY, eX)
      else:
        eX = cX - dX
        eY = cY - dY
        curr_angle  = np.arctan2(eY, eX)
    prev_angle = curr_angle
    # print( np.sign(vX), np.sign(vY), toggle)
    if toggle:
      cv2.arrowedLine(output,(int(dX),int(dY)), (int(cX),int(cY)),(0,255,0), 10 , tipLength=.4)
      # print(np.arctan2(dY-cY,dX-cX) + np.pi)
    else:
      cv2.arrowedLine(output, (int(cX),int(cY)), (int(dX),int(dY)),(0,255,0), 10 , tipLength=.4)
      # print(np.arctan2(cY-dY
      # ,cX-dX) + np.pi)
    
    
    # cv2.circle(output, (int(cX),int(cY)),3,(0,0,255),-1)
    # cv2.circle(output, (int(dX),int(dY)),3,(0,0,0),-1)
    cv2.imshow("frame",output)
    # cv2.imshow('Frame',result)
    # track.write(output)
    # segment.write(result)
    if cv2.waitKey(25) & 0xFF == ord('q'):
      break
  # elif ret == False:
  #   cap.set(2,0)
  else: 
    break

segment.release()
track.release()
cap.release()

cv2.destroyAllWindows()