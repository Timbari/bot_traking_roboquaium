from pupil_apriltags import Detector
import cv2
from imutils.video import VideoStream

at_detector = Detector(families='tag36h11',
                       nthreads=1,
                       quad_decimate=1.0,
                       quad_sigma=0.0,
                       refine_edges=1,
                       decode_sharpening=0.25,
                       debug=1)

# cap = cv2.VideoCapture(0)

# if (cap.isOpened()== False): 
#   print("Error opening video stream or file")



frame = cv2.imread("EstimateAprilTagPosesInImageExample_01.png", cv2.IMREAD_COLOR)
tag_img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
tags = at_detector.detect(tag_img, estimate_tag_pose=False, camera_params=None, tag_size=0.04)
for tag in tags:
  cv2.circle(frame, (int(tag.center[0]),int(tag.center[1])),10,(0,0,255),-1)
  cv2.putText(frame, str(tag.tag_id), (int(tag.center[0]), int(tag.center[1])-30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
  for corner in tag.corners:
    cv2.circle(frame, (int(corner[0]),int(corner[1])),10,(255,0,0),-1)
cv2.imshow("frame", frame)
cv2.imwrite("output.png", frame)
while(True):
  if cv2.waitKey(25) & 0xFF == ord('q'):
    break
cv2.destroyAllWindows()